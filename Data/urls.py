from django.urls import path
from . import views

urlpatterns = [
    path('adding/', views.adding, name='adding'),
    path('view/', views.view, name="view"),
    path('add/<int:url_ID>', views.add, name="add"),
    path('view/<int:url_ID>', views.view_album, name='view'),
    path('loginOwO/', views.loginOwO, name="loginOwO"),
    path('login/', views.login, name="login"),
    path('logout/', views.logout, name="logout"),
    path('signupOwO/', views.signupOwO, name="signupOwO"),
    path('signup/', views.signup, name='signup'),
    path('', views.index, name="index"),
]
