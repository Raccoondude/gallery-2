from django import forms

class SignupForm(forms.Form):
    Username = forms.CharField(max_length=25)
    Password = forms.CharField(max_length=50)

class LoginForm(forms.Form):
    ID = forms.IntegerField()
    Password = forms.CharField(max_length=50)

class ImageForm(forms.Form):
    Name = forms.CharField(max_length=25)
    File = forms.ImageField()
