from django.shortcuts import render, render_to_response
from .models import *
from django.utils.crypto import get_random_string
from .forms import *
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseRedirect
# Create your views here.

def Check_ID(ID, Key):
    try:
        OwO = User.objects.get(User_ID=ID)
    except Exception:
        return False
    if Key == OwO.User_key:
        return True
    else:
        return False

def index(request):
    try:
        ID = request.session['ID']
        Key = request.session['Key']
    except Exception:
        return render(request, 'Data/pls.html')
    check = Check_ID(ID, Key)
    if check == True:
        OwO = User.objects.get(User_ID=ID)
        return render(request, 'Data/index.html', {'name':OwO.User_name})
    else:
        return render(request, 'Data/pls.html')

def signup(request):
    form = SignupForm()
    return render(request, 'Data/signup.html', {'form':form})

def signupOwO(request):
    if request.method == "POST":
        MyForm = SignupForm(request.POST)
        if MyForm.is_valid():
            Key = str(get_random_string(length=50))
            Username = MyForm.cleaned_data['Username']
            Password = MyForm.cleaned_data['Password']
            OwO = User(User_name=Username, User_password=Password, User_key=Key)
            OwO.save()
            ID = OwO.User_ID
            request.session['ID'] = ''
            request.session['Key'] = ''
            return render(request, 'Data/signupdone.html', {'ID':ID, 'name':Username})

def logout(request):
    request.session['Key'] = '';
    request.session['ID'] = '';
    return HttpResponseRedirect('/')

def login(request):
    form = LoginForm()
    return render(request, 'Data/login.html', {'form':form})

def loginOwO(request):
    if request.method == "POST":
        MyForm = LoginForm(request.POST)
        if MyForm.is_valid():
            ID = MyForm.cleaned_data['ID']
            try:
                OwO = User.objects.get(User_ID=ID)
            except Exception:
                return HttpResponse("Error, <a href='/login/'>Go Back</a>")
            password = MyForm.cleaned_data['Password']
            if password == OwO.User_password:
                request.session['ID'] = ID
                request.session['Key'] = OwO.User_key
                return HttpResponseRedirect('/')
            else:
                return HttpResponse("Wrong password, <a href='/login/'>Go Back<a/>")

def view_album(request, url_ID):
    try:
        OwO = Album.objects.get(Album_ID=url_ID)
    except Exception:
        return HttpResponse("404")
    image_list = Image.objects.filter(Image_Album=OwO)
    return render(request, 'Data/viewalbum.html', {'OwO':OwO, 'image_list':image_list})

def add(request, url_ID):
    try:
        OwO = Album.objects.get(Album_ID=url_ID)
    except Exception:
        return HttpRepsonse("404")
    try:
        UserID = request.session['ID']
    except Exception:
        return HttpResponse("error 1")
    try:
        UserKey = request.session['Key']
    except Exception:
        return HttpResponse("error 2")
    call = Check_ID(UserID, UserKey)
    if call == True:
        form = ImageForm()
        request.session['Add'] = url_ID
        return  render(request, 'Data/add.html', {'form': form})
    else:
        return HttpResponse("error 3")

def view(request):
    album_list = Album.objects.all()
    return render(request, 'Data/view.html', {'album_list':album_list})

def adding(request):
    if request.method == "POST":
        MyForm = ImageForm(request.POST, request.FILES)
        if MyForm.is_valid():
            Name = MyForm.cleaned_data['Name']
            try:
                album = Album.objects.get(Album_ID=request.session['Add'])
            except Exception:
                return HttpResponse("error 1")
            try:
                UwU = User.objects.get(User_ID=request.session['ID'])
            except Exception:
                return HttpResponse("error 2")
            if UwU.User_CanUpload == False:
                return HttpResponse("You can't upload!")
            File =  request.FILES['File']
            OwO = Image(Image_img=File, Image_name=File, Image_Album=album, Image_User=UwU)
            OwO.save()
            return HttpResponseRedirect("/view/" + str(album.Album_ID))
        else:
            return HttpResponse("Valid error")
    else:
        return HttpResponse("Request error")
