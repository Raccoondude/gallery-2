from django.db import models

# Create your models here.

class User(models.Model):
    User_ID = models.AutoField(primary_key=True)
    User_name = models.CharField(max_length=25)
    User_password = models.CharField(max_length=50)
    User_key = models.CharField(max_length=100)
    User_CanMakeAlbums = models.BooleanField(default=False)
    User_CanUpload = models.BooleanField(default=True)
    def __str__(self):
        return self.User_name

class Album(models.Model):
    Album_ID = models.AutoField(primary_key=True)
    Album_name = models.CharField(max_length=25)
    Album_User = models.ForeignKey(User, on_delete=models.CASCADE)
    def __str__(self):
        return self.Album_name
    
class Image(models.Model):
    Image_ID = models.AutoField(primary_key=True)
    Image_User = models.ForeignKey(User, on_delete=models.CASCADE)
    Image_Album = models.ForeignKey(Album, on_delete=models.CASCADE)
    Image_img = models.ImageField(upload_to='OwO')
    Image_name = models.CharField(max_length=25)
    def __str__(self):
        return str(self.Image_ID)
